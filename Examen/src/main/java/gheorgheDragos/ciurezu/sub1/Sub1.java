package gheorgheDragos.ciurezu.sub1;

class K {
}

class L extends K{
    private int a;
    private M mObject;

    public void b() {
        System.out.println("b method called!");
    }

    public void i() {
        System.out.println("i method called");
    }

    public int getA() {
        return a;
    }
}

class M {
    B bObject;

    M(){
        bObject = new B();
    }
}

class B{
}

class A{
    M mObject;
    A(M mObject){
        this.mObject=mObject;
    }

}

class X{
    public void met(L l){
        System.out.println("value for \"a\" is: "+l.getA());
    }
}

