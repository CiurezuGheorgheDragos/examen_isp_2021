package gheorgheDragos.ciurezu.sub2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class GUI extends JFrame {
    private final int HEIGHT_SCREEN_SIZE = 250;
    private final int WIDTH_SCREEN_SIZE = 300;
    private final int WIDTH_COMPONENTS = 100;
    private final int HEIGHT_COMPONENTS = 20;

    private JTextField firstInputTextField;
    private JTextField secondInputTextField;
    private JTextField resultTextField;

    private JLabel firstInputLabel;
    private JLabel secondInputLabel;
    private JLabel resultLabel;

    private JButton calculateButton;

    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(WIDTH_SCREEN_SIZE, HEIGHT_SCREEN_SIZE);
        this.setLayout(null);
        this.setTitle("Subject 2");

        this.init();
    }

    private void init() {
        firstInputLabel = new JLabel("First Input:");
        firstInputLabel.setBounds(40, 40, WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        firstInputTextField = new JTextField();
        firstInputTextField.setBounds(firstInputLabel.getX() + WIDTH_COMPONENTS, firstInputLabel.getY(),
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        secondInputLabel = new JLabel("Second Input");
        secondInputLabel.setBounds(firstInputLabel.getX(), firstInputLabel.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        secondInputTextField = new JTextField();
        secondInputTextField.setBounds(secondInputLabel.getX() + WIDTH_COMPONENTS, secondInputLabel.getY(),
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        calculateButton = new JButton("Calculate");
        calculateButton.setBounds(secondInputTextField.getX() - 40, secondInputTextField.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS + 40, HEIGHT_COMPONENTS);
        calculateButton.addActionListener(new CalculateButtonFunctionality());

        resultLabel = new JLabel("Result:");
        resultLabel.setBounds(firstInputLabel.getX(), calculateButton.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        resultTextField = new JTextField();
        resultTextField.setBounds(resultLabel.getX() + WIDTH_COMPONENTS, resultLabel.getY(),
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);
        resultTextField.setEditable(false);

        add(firstInputLabel);
        add(firstInputTextField);
        add(secondInputLabel);
        add(secondInputTextField);
        add(calculateButton);
        add(resultLabel);
        add(resultTextField);

        setVisible(true);
    }



    private class CalculateButtonFunctionality implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String firstField = firstInputTextField.getText();
            String secondField = secondInputTextField.getText();

            try {
                double firstNumber = Double.parseDouble(firstField);
                double secondNumber = Double.parseDouble(secondField);
                double result = firstNumber * secondNumber;

                resultTextField.setText(String.valueOf(result));
            } catch (NumberFormatException numberFormatException) {
                String messageError = "You should put only numbers in the first and second field to " +
                        "calculate the product.\n Try Again!";

                JOptionPane.showMessageDialog(GUI.this, messageError);
                resultTextField.setText("Invalid!");
            }

        }
    }
}


class Main {
    public static void main(String[] args) {
        new GUI();
    }
}

